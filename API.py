from moviepy.editor import *
from flask import Flask
from flask import request
from flask_cors import CORS
import os
from flask import send_file

preferred_frame_width = 500

app = Flask(__name__)
cors = CORS(app)

@app.route('/upload', methods=['POST'])
def upload():
    f = request.files['file']
    splits = f.filename.split("/")
    f.save(os.path.join("", "upload.mov"))
    clip = (VideoFileClip("upload.mov"))
    resizePercentage = preferred_frame_width / clip.size[0]
    clip = clip.resize(resizePercentage)
    clip.write_gif("output.gif", fps=10)
    return send_file("output.gif", mimetype='image/gif')

if __name__ == '__main__':
    app.run(debug=False, port=5000, host="0.0.0.0")
