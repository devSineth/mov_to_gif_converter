from moviepy.editor import *

preferred_frame_width = 500

clip = VideoFileClip("file_example_MOV_1920_2_2MB.mov")
resizePercentage = preferred_frame_width / clip.size[0]
clip = clip.resize(resizePercentage)
# clip.write_gif("output.gif", fps=10)
